import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.jgrapht.alg.interfaces.MinimumSpanningTree;

public class Flight {
	private int flightNum;
	private String oriAirport;
	private String desAirport;
	private LocalDateTime depTime;
	private LocalDateTime arrTime;
	private DateTimeFormatter formatter;

	public Flight(int flightNum, String oriAirport, String desAirport, LocalDateTime depTime, LocalDateTime arrTime) {
		this.flightNum = flightNum;
		this.oriAirport = oriAirport;
		this.desAirport = desAirport;
		this.depTime = depTime;
		this.arrTime = arrTime;
		this.formatter = DateTimeFormatter.ofPattern("EEE, HH:mm");
	}

	public Integer getFlightNum() {
		return flightNum;
	}

	public String getOriAirport() {
		return oriAirport;
	}

	public String getDesAirport() {
		return desAirport;
	}
	
	public LocalDateTime getDepTime(){
		return this.depTime;
	}
	
	public String getDepTimeInString() {
		return formatter.format(this.depTime);
	}
	
	public LocalDateTime getArrTime(){
		return this.arrTime;
	}

	public String getArrTimeInString() {
		return formatter.format(this.arrTime);
	}

	public long getFlightDuration() {
		Duration duration;
		if (depTime.isBefore(arrTime)) {
			duration = Duration.between(depTime, arrTime);
		} else if (depTime.equals(arrTime)) {
			duration = Duration.ofHours(24);
		} else {
			duration = Duration.between(depTime, arrTime.plusSeconds(86400));
		}
		return duration.abs().toMinutes();
	}
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("Flight Num: " + this.flightNum + "\n");
		builder.append("Departure Airport: " + this.getOriAirport() + "\n");
		builder.append("Arrival Airport: " + this.getDesAirport() + "\n");
		builder.append("Departure Time: " + this.getDepTimeInString() + "\n");
		builder.append("Arrival Time: " + this.getArrTimeInString() + "\n");
		builder.append("Flight Duration: " + this.getFlightDuration() + " minutes\n");
		return builder.toString();
	}
}
