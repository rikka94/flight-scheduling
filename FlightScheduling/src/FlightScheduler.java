import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.print.attribute.Size2DSyntax;

import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

public class FlightScheduler {
	private HashMap<String, Integer> airportRefTable; // map airport name to
														// airport id
	private HashMap<Integer, Airport> airportList; // map airport id to airport
	private HashMap<FlightKey, Flight> flightSchedule; // map flight custom key
														// to flight
	// Complete graph without filtering
	private SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> completeFlightGraph;
	private DataReader reader;

	public FlightScheduler() {
		reader = new DataReader();
		ArrayList<Airport> airports = reader.getAirport();
		ArrayList<Flight> flights = reader.getFlight(); // read data from text
														// file

		airportRefTable = new HashMap<String, Integer>();
		airportList = new HashMap<Integer, Airport>();
		flightSchedule = new HashMap<FlightKey, Flight>();

		mapAirportRefTable(airports);
		mapAirportList(airports);
		mapFlightSchedule(flights);

		completeFlightGraph = initializeGraph(airports, flights);
	}

	// Initialization of HashMap
	private void mapAirportRefTable(ArrayList<Airport> airports) {
		for (Airport airport : airports) {
			this.airportRefTable.put(airport.getName(), airport.getId());
		}
	}

	private void mapAirportList(ArrayList<Airport> airports) {
		for (Airport airport : airports) {
			this.airportList.put(airport.getId(), airport);
		}
	}

	private void mapFlightSchedule(ArrayList<Flight> flights) {
		for (Flight flight : flights) {
			FlightKey key = new FlightKey(flight.getOriAirport(), flight.getDesAirport());
			this.flightSchedule.put(key, flight);
		}
	}

	// return a specific airport according to id
	public Airport getAirport(int id) {
		return airportList.get(id);
	}

	// Return a array list contains all airports in hashmap
	public ArrayList<Airport> getAirportList() {
		ArrayList<Airport> list = new ArrayList<Airport>();
		for (Entry<Integer, Airport> entry : airportList.entrySet()) {
			list.add(entry.getValue());
		}
		return list;
	}

	// return a linked-list contains all flight in hashmap
	// (this will be mainly use for elements removal)
	public List<Flight> getFlightList() {
		List<Flight> list = new LinkedList<Flight>();
		for (Entry<FlightKey, Flight> entry : flightSchedule.entrySet()) {
			list.add(entry.getValue());
		}
		return list;
	}

	// Create and return a new weighted directed graph
	private SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> initializeGraph(List<Airport> airports,
			List<Flight> flights) {

		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> flightGraph = new SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>(
				DefaultWeightedEdge.class);

		// Adding list of airport into graph as vertices
		for (int i = 0; i < airports.size(); i++) {
			flightGraph.addVertex(airports.get(i).getName());
		}

		DefaultWeightedEdge edge = new DefaultWeightedEdge();
		for (int i = 0; i < flights.size(); i++) {
			// Add edges into the graph based on flight departure and arrival
			// airport
			edge = flightGraph.addEdge(flights.get(i).getOriAirport(), flights.get(i).getDesAirport());
			// Minimum connecting time of departure airport
			long minCt = getAirport(airportRefTable.get(flights.get(i).getOriAirport())).getMinCT().toMinutes();

			// Set the weight of each edges to the total flight duration (min
			// connecting time will be subtracted
			// from source and destination in the end of path computation)
			flightGraph.setEdgeWeight(edge, flights.get(i).getFlightDuration() + minCt);
		}
		return flightGraph;
	}

	// Filter an airport list by removing departure airport
	private List<Flight> getFilteredFlights(String oriAirport, String desAirport, int dayOfWeek, LocalTime time) {
		// Linked list of all flights ready to filtering
		List<Flight> unfilteredFlights = getFlightList();
		List<Flight> flightToRemove = new ArrayList<Flight>();
		int currAirportId;
		for (Flight flight : unfilteredFlights) {
			currAirportId = airportRefTable.get(flight.getOriAirport());
			// hour and minute will be only considered for the first day of week
			boolean timeFactor = true;

			// In the user input time, if the particular flight on the day is
			// earlier than
			// user inputted time (in term of minutes and hour)
			if (time != null) {
				if (flight.getDepTime().getHour() >= time.getHour()) {
					if (flight.getDepTime().getMinute() >= time.getMinute()) {
						timeFactor = false;
					}
				}
			}

			// remove all the flight on departure day of the departure airport
			// not equals to selected day of week
			if (flight.getDepTime().getDayOfWeek().getValue() != dayOfWeek && timeFactor
					&& airportList.get(currAirportId).getName() == oriAirport) {
				flightToRemove.add(flight);
			}
		}
		unfilteredFlights.removeAll(flightToRemove);
		return unfilteredFlights;
	}

	public ArrayList<List<DefaultWeightedEdge>> computePath(int oriAirport, int desAirport, DayOfWeek dayOfWeek,
			LocalTime minTime) {
		ArrayList<List<DefaultWeightedEdge>> listOfPath = new ArrayList<List<DefaultWeightedEdge>>();
		String oriAirportName = getAirport(oriAirport).getName();
		String desAirportName = getAirport(desAirport).getName();
		int day = dayOfWeek.getValue();
		LocalTime time = minTime;
		for (int i = 0; i < 7; i++) {
			List<DefaultWeightedEdge> shortestPath = DijkstraShortestPath.findPathBetween(
					// Generate a new graph with filtered flights based on user
					// input time
					// Each Loop will increment day by one (longer departure
					// time)
					initializeGraph(getAirportList(), getFilteredFlights(oriAirportName, desAirportName, day, time)),
					// origin vertex and destination vertex
					getAirport(oriAirport).getName(), getAirport(desAirport).getName());
			listOfPath.add(shortestPath);

			// if day of week is 7(sunday) revert it to 1 (monday)
			if (day > 7)
				day = 1;
			else
				day++;

			// After first loop (first day after user input day) , set the time
			// to null
			// to make sure no time factor is taken into account
			if (i != 0)
				time = null;
		}
		return listOfPath;
	}

	// convert from vertices into actual flight information for all the shortest
	// path for the whole week
	public List<List<Flight>> computeAllPossibleTrip(ArrayList<List<DefaultWeightedEdge>> listOfPath) {
		List<List<Flight>> allTrips = new ArrayList<List<Flight>>();
		FlightKey key = null;
		for (int i = 0; i < listOfPath.size(); i++) {
			if (listOfPath.get(i) != null) {
				allTrips.add(new ArrayList<Flight>());
				for (DefaultWeightedEdge edge : listOfPath.get(i)) {
					String source = completeFlightGraph.getEdgeSource(edge);
					String destination = completeFlightGraph.getEdgeTarget(edge);
					key = new FlightKey(source, destination);
					allTrips.get(allTrips.size()-1).add(flightSchedule.get(key));
				}
			}
		}
		return allTrips;
	}

	// get the earliest possible trip by extract the first element from the list
	public List<Flight> computeEarliestPossibleTrip(ArrayList<List<DefaultWeightedEdge>> listOfPath) {
		List<Flight> earliestFlight = new ArrayList<Flight>();
		FlightKey key;
		for (int i = 0; i < listOfPath.size(); i++) {
			if(listOfPath.get(i) == null)
				continue;
			
			for (DefaultWeightedEdge edge : listOfPath.get(i)) {
				String source = completeFlightGraph.getEdgeSource(edge);
				String destination = completeFlightGraph.getEdgeTarget(edge);
				key = new FlightKey(source, destination);
				earliestFlight.add(flightSchedule.get(key));
			}
			return earliestFlight;
		}
		return null;
	}

	// Compute the shortest possible trip by flight duration + minimum
	// connecting time factor
	// Not including the arrival time
	public List<Flight> computeShortestPossibleTrip(ArrayList<List<DefaultWeightedEdge>> listOfPath) {
		List<DefaultWeightedEdge> shortest = null;
		int shortestCost = Integer.MAX_VALUE;
		for (List<DefaultWeightedEdge> path : listOfPath) {
			int totalCost = 0;
			if (path != null) {
				for (DefaultWeightedEdge edge : path) {
					totalCost += completeFlightGraph.getEdgeWeight(edge);
				}
				if (shortestCost >= totalCost) {
					shortestCost = totalCost;
					shortest = path;
				}
			}
		}
		List<Flight> shortestFlight = new ArrayList<>();
		FlightKey key;
		for (DefaultWeightedEdge edge : shortest) {
			String source = completeFlightGraph.getEdgeSource(edge);
			String destination = completeFlightGraph.getEdgeTarget(edge);
			key = new FlightKey(source, destination);
			shortestFlight.add(flightSchedule.get(key));
		}
		return shortestFlight;
	}
}
