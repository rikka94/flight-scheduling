import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.jgrapht.graph.DefaultWeightedEdge;

public class MainProgram {

	public static void main(String[] args) {
		FlightScheduler flightScheduler = new FlightScheduler();

		Scanner sc = new Scanner(System.in);
		boolean terminateProgram = false;
		while (!terminateProgram) {
			try {
				welcomeScreenMessage();
				printAllAirport(flightScheduler.getAirportList());

				System.out.print("Select ID of the departure airport from the list :");
				Integer depAirport = Integer.parseInt(sc.nextLine());

				System.out.print("Select ID of the destination airport from the list :");
				Integer arrAirport = Integer.parseInt(sc.nextLine());
				

				System.out.println("Enter your prefered departure day (Mon/Monday, Tue/Tuesday ,Wed/Wednesday etc..)");
				String inputDay = sc.nextLine();
				DayOfWeek day = validateDayofWeek(inputDay);

				System.out.println("Enter your prefered departure time (12:30, 2:33, 7:54 etc..)");
				String time = sc.nextLine();
				LocalTime minTime = validateTime(time);
				ArrayList<List<DefaultWeightedEdge>> listOfRoute = flightScheduler.computePath(depAirport, arrAirport, day, minTime);
				System.out.println("All Possible Flight");
				List<List<Flight>> allFlight = flightScheduler.computeAllPossibleTrip(listOfRoute);
				int x = 1;
				for(List<Flight> route: allFlight){
					if(route.size()!=0){
						System.out.println("Flight Path "+x);
						x++;
					for(Flight flight : route){
						System.out.println(flight);
						
					}
					}
				}
				
				List<Flight> earliestFlight = flightScheduler.computeEarliestPossibleTrip(listOfRoute);
				if(earliestFlight.size()!=0){
					System.out.println("Earliest Flight Available");
				for(Flight route: earliestFlight){
					System.out.println(route);
					}}
				
				
				
				List<Flight> shortestFlight = flightScheduler.computeShortestPossibleTrip(listOfRoute);
				if(shortestFlight.size()!=0){
					System.out.println("Shortest Flight Avaiable");
				for(Flight route: shortestFlight){
					
						System.out.println(route);
					
				}}
				
				
				terminateProgram = shouldContinue(sc);

			} catch (NumberFormatException e) {
				clearScreen();
				System.out.println("Only digit values is accepted!");
				continue;
			} catch (IllegalArgumentException e) {
				clearScreen();
				System.out.println(e.getMessage());
				continue;
			}
		}
		exitProgramMessage();
	}

	public static void exitProgramMessage() {
		String exitMessage = "Thank you for using our flight scheduler ! Goodbye ~~ ^_^";
		System.out.println(exitMessage);
	}

	public static void welcomeScreenMessage() {
		String welcomeMessage = "WELCOME TO FLIGHT SCHEDULE HELPER PROGRAM \n"
				+ "----------------------------------------------------------------------------------------";
		System.out.println(welcomeMessage);
	}

	public static void printAllAirport(ArrayList<Airport> airports) {
		System.out.printf(
				"%4s %-40s %-10s\n------------------------------------------------------------------------------------\n",
				"Airport ID", "Airport Name", "Minimum Connecting Time");
		for (Airport airport : airports) {
			System.out.printf("%4s %-60s %-10s\n", airport.getId(), airport.getName(), airport.getMinCT().toMinutes());
		}
	}

	public static void clearScreen() {
		for (int i = 0; i < 500; i++) {
			System.out.println("\b");
		}
	}

	public static boolean shouldContinue(Scanner sc) {
		System.out.println("Do you want to attempt again ?? Y to continue /Any to terminate session ~~");
		String input = sc.nextLine();
		if (input.equals("Y") || input.equals("y")) {
			clearScreen();
			return false;
		}
		return true;
	}
	

	public static DayOfWeek validateDayofWeek(String dayOfWeek) {
		switch (dayOfWeek.toLowerCase()) {
		case "mon":
		case "monday":
			return DayOfWeek.MONDAY;
		case "tue":
		case "tuesday":
			return DayOfWeek.TUESDAY;
		case "wed":
		case "wednesday":
			return DayOfWeek.WEDNESDAY;
		case "thu":
		case "thursday":
			return DayOfWeek.THURSDAY;
		case "fri":
		case "friday":
			return DayOfWeek.FRIDAY;
		case "sat":
		case "saturday":
			return DayOfWeek.SATURDAY;
		case "sun":
		case "sunday":
			return DayOfWeek.SUNDAY;
		default:
			throw new IllegalArgumentException("Invalid Day of weeks");
		}
	}

	public static LocalTime validateTime(String time) {
		if (time.matches("([01]?[0-9]|2[0-3]):[0-5][0-9]")) {
			String[] tokens = time.split(":");
			return LocalTime.of(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]));
		}
		throw new IllegalArgumentException("Invalid 24-hours time foramt");
	}
}
