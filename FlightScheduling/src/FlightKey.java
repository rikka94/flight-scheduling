import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
public class FlightKey {
	private String source;
	private String destination;
	
	public FlightKey(String source, String destination){
		this.source = source;
		this.destination = destination;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof FlightKey){
			FlightKey key = (FlightKey) obj;
			EqualsBuilder builder = new EqualsBuilder();
			builder.append(this.source, key.source);
			builder.append(this.destination, key.destination);
			return builder.isEquals();
		}
		return false;
	};
	
	@Override
	public int hashCode(){
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(this.source);
        builder.append(this.destination);
        return builder.toHashCode();
    }
}
