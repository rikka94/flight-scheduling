import java.io.*;
import java.time.LocalDateTime;
import java.time.Duration;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;


public class DataReader {
	private File airportData;
	private File flightData;
	
	public DataReader(){
		airportData = new File("airportsv3.txt");
		flightData = new File("flightsv3.txt");
	}
	
	public ArrayList<Airport> getAirport(){
		//Initialization
		ArrayList<Airport> airports = new ArrayList<Airport>();
		String[] strs;
		Airport airport;
		Scanner sc = null;
		//Start reading the file
		try {
			String line;
			sc = new Scanner(airportData);
			while(sc.hasNextLine()){
				line = sc.nextLine();
				strs = line.split(",");
				airport = new Airport(Integer.parseInt(strs[0]), strs[1], Duration.ofMinutes(Long.parseLong(strs[2])));
				airports.add(airport);
			}			
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}
		finally{
			sc.close();
		}
		return airports;
	}
	
	public ArrayList<Flight> getFlight(){
		ArrayList<Flight> flights = new ArrayList<Flight>();  //ArrayList of flight to return to the main controller
		String[] strs; //Array of string to hold the partition of value from readline temporarily
		Flight flight;
		Scanner sc = null;
		try {
			String line;
			sc = new Scanner(flightData);
			LocalDateTime depTime, arrTime;
			String[] depDate, arrDate;     //Date in text file in format HH:mm/DAY OF WEEK
			int[] date;
			int hour, minute;
			Calendar temp;
			
			while(sc.hasNextLine()){
				line = sc.nextLine();
				//Post-processing the read line from text file 
				strs = line.split(",");
				depDate = strs[3].split("/");
				arrDate = strs[4].split("/");
				
				//Departure
				temp = Calendar.getInstance();
				hour = Integer.parseInt(depDate[0].split(":")[0]);
				minute = Integer.parseInt(depDate[0].split(":")[1]);
				temp.set(Calendar.DAY_OF_WEEK, getDay(depDate[1]));
				temp.set(Calendar.HOUR, hour);
				temp.set(Calendar.MINUTE, minute);
				depTime = LocalDateTime.ofInstant(temp.toInstant(), ZoneId.systemDefault());
				
				//Arrival
				temp = Calendar.getInstance();
				hour = Integer.parseInt(arrDate[0].split(":")[0]);
				minute = Integer.parseInt(arrDate[0].split(":")[1]);
				temp.set(Calendar.DAY_OF_WEEK, getDay(arrDate[1]));
				temp.set(Calendar.HOUR, hour);
				temp.set(Calendar.MINUTE, minute);
				arrTime = LocalDateTime.ofInstant(temp.toInstant(), ZoneId.systemDefault());
				
				flight = new Flight(Integer.parseInt(strs[0]), strs[1], strs[2], depTime, arrTime);
				flights.add(flight);
			}
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}
		finally{
			sc.close();
		}
		return flights;
	}
	
	public static int getDay(String day) {
	    if (day == null) {
	        return -1;
	    }
	    switch (day) {
	        case "Monday":
	            return Calendar.MONDAY;
	        case "Tuesday":
	        	return Calendar.TUESDAY;
	        case "Wednesday":
	        	return Calendar.WEDNESDAY;
	        case "Thursday":
	        	return Calendar.THURSDAY;
	        case "Friday":
	        	return Calendar.FRIDAY;
	        case "Saturday":
	        	return Calendar.SATURDAY;
	        case "Sunday":
	        	return Calendar.SUNDAY;
	        default: 
	        	return -1;
	    }
	}
}
