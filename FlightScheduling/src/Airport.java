import java.time.Duration;

public class Airport {
	private int id;
	private String name;
	private Duration minCT;  			//Minimum connecting time
	
	public Airport(int id, String name, Duration minCT){
		this.id = id;
		this.name = name;
		this.minCT = minCT;
	}
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public Duration getMinCT(){
		return minCT;
	}
	
}
