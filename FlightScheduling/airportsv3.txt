0,Frankfurt am Main International Airport,55
1,Hamburg Airport,180
2,Cologne Bonn Airport,55
3,Modlin Airport,105
4,Warsaw Chopin Airport,35
5,Gothenburg-Landvetter Airport,160
6,Stockholm-Arlanda Airport,110
7,Ramstein Air Base,75
8,Riga International Airport,95
9,Port Columbus International Airport,165
10,Montgomery Regional (Dannelly Field) Airport,120
11,Manchester Airport,180
12,Miami International Airport,145
13,General Mitchell International Airport,65
14,Quad City International Airport,100
15,Monroe Regional Airport,65
16,Mobile Regional Airport,175
17,Dane County Regional Truax Field,125
18,Minneapolis-St Paul International/Wold-Chamberlain Airport,160
19,Louis Armstrong New Orleans International Airport,110
20,Mountain Home Air Force Base,165
21,Metropolitan Oakland International Airport,120
22,Heraklion International Nikos Kazantzakis Airport,150
23,Thessaloniki Macedonia International Airport,135
24,Budapest Ferenc Liszt International Airport,175
25,Fukuoka Airport,75
26,Kagoshima Airport,95
27,Chubu Centrair International Airport,95
28,Mt. Fuji Shizuoka Airport,35
29,Osaka International Airport,30
30,Tokyo International Airport,100
31,Kuala Lumpur International Airport,50
32,Singapore Changi International Airport,155
33,Taoxian Airport,150
